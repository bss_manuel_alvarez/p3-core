
spool 00_P3TOOL_CREATE_USERS.log

--DROP USER P3TOOL_CONFIG CASCADE;

CREATE USER P3TOOL_CONFIG IDENTIFIED BY welcome1
 DEFAULT TABLESPACE users
 TEMPORARY TABLESPACE temp
 QUOTA UNLIMITED ON users;

GRANT create session
    , create table
    , create procedure
    , create sequence
    , create trigger
    , create view
    , create synonym
    , alter session
    , create type
    , create materialized view
    , query rewrite
    , create dimension
    , create any directory
    , alter user
    , resumable
    , ALTER ANY TABLE  -- These
    , DROP ANY TABLE   -- five are
    , LOCK ANY TABLE   -- needed
    , CREATE ANY TABLE -- to use
    , SELECT ANY TABLE -- DBMS_REDEFINITION
TO P3TOOL_CONFIG;

GRANT select_catalog_role
    , execute_catalog_role
TO P3TOOL_CONFIG;

--DROP USER P3TOOL_RUNTIME CASCADE;

CREATE USER P3TOOL_RUNTIME IDENTIFIED BY welcome1
 DEFAULT TABLESPACE users
 TEMPORARY TABLESPACE temp
 QUOTA UNLIMITED ON users;

GRANT create session
    , create table
    , create procedure
    , create sequence
    , create trigger
    , create view
    , create synonym
    , alter session
    , create type
    , create materialized view
    , query rewrite
    , create dimension
    , create any directory
    , alter user
    , resumable
    , ALTER ANY TABLE  -- These
    , DROP ANY TABLE   -- five are
    , LOCK ANY TABLE   -- needed
    , CREATE ANY TABLE -- to use
    , SELECT ANY TABLE -- DBMS_REDEFINITION
TO P3TOOL_RUNTIME;

GRANT select_catalog_role
    , execute_catalog_role
TO P3TOOL_RUNTIME;

spool off
