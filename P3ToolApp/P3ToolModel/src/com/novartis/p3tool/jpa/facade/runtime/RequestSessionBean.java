package com.novartis.p3tool.jpa.facade.runtime;

import com.novartis.p3tool.jpa.model.runtime.Request;

import java.util.List;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name = "RequestSession", mappedName = "P3ToolApp-P3ToolModel-RequestSession")
public class RequestSessionBean implements RequestSession {
    @PersistenceContext(unitName="P3ToolRuntime")
    private EntityManager em;

    public RequestSessionBean() {
    }

    public Request persistRequest(Request request) {
        em.persist(request);
        return request;
    }

    public Request mergeRequest(Request request) {
        return em.merge(request);
    }

    public void removeRequest(Request request) {
        request = em.find(Request.class, request.getRequestId());
        em.remove(request);
    }

    /** <code>select o from Request o</code> */
    public List<Request> getRequestFindAll() {
        return em.createNamedQuery("Request.findAll").getResultList();
    }
}
