package com.novartis.p3tool.jpa.facade.runtime;

import com.novartis.p3tool.jpa.model.runtime.Act;

import java.util.List;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name = "ActSession", mappedName = "P3ToolApp-P3ToolModel-ActSession")
public class ActSessionBean implements ActSession {
    @PersistenceContext(unitName="P3ToolRuntime")
    private EntityManager em;

    public ActSessionBean() {
    }

    public Act persistAct(Act act) {
        em.persist(act);
        return act;
    }

    public Act mergeAct(Act act) {
        return em.merge(act);
    }

    public void removeAct(Act act) {
        act = em.find(Act.class, act.getActId());
        em.remove(act);
    }

    /** <code>select o from Act o</code> */
    public List<Act> getActFindAll() {
        return em.createNamedQuery("Act.findAll").getResultList();
    }




}
