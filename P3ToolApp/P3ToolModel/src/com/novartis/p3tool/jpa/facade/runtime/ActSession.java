package com.novartis.p3tool.jpa.facade.runtime;

import com.novartis.p3tool.jpa.model.runtime.Act;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface ActSession {
    Act persistAct(Act act);

    Act mergeAct(Act act);

    void removeAct(Act act);

    List<Act> getActFindAll();


}
