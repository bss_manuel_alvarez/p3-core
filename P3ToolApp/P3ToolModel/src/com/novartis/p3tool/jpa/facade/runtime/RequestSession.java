package com.novartis.p3tool.jpa.facade.runtime;

import com.novartis.p3tool.jpa.model.runtime.Request;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface RequestSession {
    Request persistRequest(Request request);

    Request mergeRequest(Request request);

    void removeRequest(Request request);

    List<Request> getRequestFindAll();
}
