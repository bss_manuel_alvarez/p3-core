package com.novartis.p3tool.jpa.serializer;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.TimeZone;


/**
 * Used to serialize Java.util.Date, which is not a common JSON
 * type, so we have to create a custom serialize method;.
 *
 * @author Loiane Groner
 * http://loianegroner.com (English)
 * http://loiane.com (Portuguese)
 */
public class JsonDateSerializer extends JsonSerializer<Timestamp> {
  //private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

  @Override
  public void serialize(Timestamp date, JsonGenerator gen, SerializerProvider provider)
    throws IOException, JsonProcessingException {

    String formattedDate = ISO8601Utils.format(date, false, TimeZone.getDefault());

    //String formattedDate = dateFormat.format(date);

    gen.writeString(formattedDate);
  }

}
