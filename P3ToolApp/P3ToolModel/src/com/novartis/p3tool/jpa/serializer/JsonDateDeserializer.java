package com.novartis.p3tool.jpa.serializer;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.sql.Timestamp;
import java.text.ParsePosition;
import java.text.ParseException;
import java.util.Date;

public class JsonDateDeserializer extends JsonDeserializer<Timestamp> {

  public Timestamp deserialize(JsonParser parser, DeserializationContext context) {
    Timestamp result = null;
    try {
      final String value = parser.getText();
      Date date = null;

      if (value != null && value.length() > 0) {
        try {
          date = ISO8601Utils.parse(value, new ParsePosition(0));
        }
        catch (ParseException pe) {
          System.err.println("Could not parse date with format '" + value + "'");
        }
      }

      if (date != null) {
        result = new Timestamp(date.getTime());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }
}
