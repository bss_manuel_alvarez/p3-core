package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "Tenant.findAll", query = "select o from Tenant o")
})
@Table(name = "P3TOOL_TENANT")
public class Tenant implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="TENANT_ID", nullable = false)
    private Long tenantId;
    
    @Column(name="DEFAULT_LANGUAGE")
    private String defaultLanguage;
    @Column(name="DEFAULT_TIMEZONE")
    private String defaultTimezone;
    @Column(name="FULL_VISIBILITY")
    private Long fullVisibility;
    @Column(name="SHORT_NAME")
    private String shortName;
    @Column(name="TENANT_NAME", nullable = false)
    private String tenantName;
    private String theme;

    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    
    /****** Entity Relationship ******/
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "tenant")
    private List<Category> categoryList;

    public Tenant() {
    }

    public Tenant(Timestamp dateCreation, Timestamp dateModified,
                  String defaultLanguage, String defaultTimezone, Long deleted,
                  Long fullVisibility, String shortName, Long tenantId,
                  String tenantName, String theme, String userCreation,
                  String userModified) {
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.defaultLanguage = defaultLanguage;
        this.defaultTimezone = defaultTimezone;
        this.deleted = deleted;
        this.fullVisibility = fullVisibility;
        this.shortName = shortName;
        this.tenantId = tenantId;
        this.tenantName = tenantName;
        this.theme = theme;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }
    
    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }
    
    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public String getDefaultTimezone() {
        return defaultTimezone;
    }

    public void setDefaultTimezone(String defaultTimezone) {
        this.defaultTimezone = defaultTimezone;
    }
    
    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getFullVisibility() {
        return fullVisibility;
    }

    public void setFullVisibility(Long fullVisibility) {
        this.fullVisibility = fullVisibility;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
    
    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }
    
    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }
    
    @JsonIgnore
    @XmlTransient 
    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public Category addCategory(Category category) {
        getCategoryList().add(category);
        category.setTenant(this);
        return category;
    }

    public Category removeCategory(Category category) {
        getCategoryList().remove(category);
        category.setTenant(null);
        return category;
    }
}
