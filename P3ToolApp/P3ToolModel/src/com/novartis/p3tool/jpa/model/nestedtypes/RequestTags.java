package com.novartis.p3tool.jpa.model.nestedtypes;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

import java.util.List;

public class RequestTags implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    List<TagPlaceholder> tagList;

    public RequestTags() {
        super();
    }

    public void setTagList(List<TagPlaceholder> tagList) {
        this.tagList = tagList;
    }

    public List<TagPlaceholder> getTagList() {
        return tagList;
    }
    
    public static String serializeRequestTags(RequestTags requestTags) {
      
        String result = "";
        
        if(requestTags != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                result = mapper.writeValueAsString(requestTags);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    public static RequestTags deserializeRequestTags(String requestTagsStr) {
      
        RequestTags requestTags = null;

        if (requestTagsStr != null && !requestTagsStr.equals("")) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                requestTags = mapper.readValue(requestTagsStr, RequestTags.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // return
        return requestTags;
    }
    
    @Override
    public String toString() {
        
        String result = "";
        
        if(this.tagList != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                result = mapper.writeValueAsString(tagList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
}
