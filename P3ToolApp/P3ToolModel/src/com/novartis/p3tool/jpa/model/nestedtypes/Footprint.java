package com.novartis.p3tool.jpa.model.nestedtypes;

import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.Serializable;

import java.util.List;

public class Footprint implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    List<UserData> userList;
    
    public Footprint() {
        super();
    }
    
    public Footprint(List<UserData> userList) {
        super();
        this.userList = userList;
    }

    
    public void setUserList(List<UserData> userList) {
        this.userList = userList;
    }

    public List<UserData> getUserList() {
        return userList;
    }
    
    public static String serializeFootprint(Footprint footprint) {
      
        String result = "";
        
        if(footprint != null){               
            ObjectMapper mapper = new ObjectMapper();
            try {
                result = mapper.writeValueAsString(footprint);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    public static Footprint deserializeFootprint(String footprintString) {
      
        Footprint footprint = null;
        
        if (footprintString != null && !footprintString.equals("")) {    
            try {
                ObjectMapper mapper = new ObjectMapper();
                footprint = mapper.readValue(footprintString, Footprint.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return footprint;
    }
    
    
}
