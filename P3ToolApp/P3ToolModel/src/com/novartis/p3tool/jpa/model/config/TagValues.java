package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "TagValues.findAll", query = "select o from TagValues o")
})
@Table(name = "P3TOOL_TAG_VALUES")
public class TagValues implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="TAG_VALUE_ID", nullable = false)
    private Long tagValueId;
    @Column(name="TAG_VALUE", nullable = false)
    private String tagValue;
    
    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    
    /****** Entity Relationship ******/
    @ManyToOne
    @JoinColumn(name = "TAG_ID", insertable=false, updatable=false)
    private Tag tag;
        
    @Column(name="TAG_ID")
    private Long tagId;

    public TagValues() {
    }

    public TagValues(Timestamp dateCreation, Timestamp dateModified,
                     Long deleted, Long tagId, String tagValue,
                     Long tagValueId, String userCreation,
                     String userModified) {
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.tagId = tagId;
        this.tagValue = tagValue;
        this.tagValueId = tagValueId;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient  
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient  
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient  
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }


    public String getTagValue() {
        return tagValue;
    }

    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }

    public Long getTagValueId() {
        return tagValueId;
    }

    public void setTagValueId(Long tagValueId) {
        this.tagValueId = tagValueId;
    }

    @JsonIgnore
    @XmlTransient  
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient  
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient  
    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
