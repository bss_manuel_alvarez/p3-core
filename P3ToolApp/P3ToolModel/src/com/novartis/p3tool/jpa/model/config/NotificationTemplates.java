package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "NotificationTemplates.findAll", query = "select o from NotificationTemplates o")
})
@Table(name = "P3TOOL_NOTIFICATION_TEMPLATES")
public class NotificationTemplates implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Column(name="LANGUAGE_LOC")
    private String languageLoc;
    @Column(name="TEMPLATE_CONTENT", length = 4000)
    private String templateContent;
    @Id
    @Column(name="TEMPLATE_ID", nullable = false)
    private Long templateId;
    @Column(name="TEMPLATE_SUBJECT", nullable = false)
    private String templateSubject;
    @Column(name="TEMPLATE_TYPE", nullable = false, length = 50)
    private String templateType;
    
    /* Auditory fields */  
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;

    public NotificationTemplates() {
    }

    public NotificationTemplates(Timestamp dateCreation,
                                 Timestamp dateModified, Long deleted,
                                 String languageLoc, String templateContent,
                                 Long templateId, String templateSubject,
                                 String templateType, String userCreation,
                                 String userModified) {

        this.languageLoc = languageLoc;
        this.templateContent = templateContent;
        this.templateId = templateId;
        this.templateSubject = templateSubject;
        this.templateType = templateType;
        this.userCreation = userCreation;
        this.userModified = userModified;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getLanguageLoc() {
        return languageLoc;
    }

    public void setLanguageLoc(String languageLoc) {
        this.languageLoc = languageLoc;
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getTemplateSubject() {
        return templateSubject;
    }

    public void setTemplateSubject(String templateSubject) {
        this.templateSubject = templateSubject;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }
}
