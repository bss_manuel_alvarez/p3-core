package com.novartis.p3tool.jpa.model.runtime;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.novartis.p3tool.jpa.serializer.JsonDateDeserializer;
import com.novartis.p3tool.jpa.serializer.JsonDateSerializer;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@NamedQueries({
  @NamedQuery(name = "ActType.findAll", query = "select o from ActType o")
})
@Table(name = "ACT_TYPE")
public class ActType implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="ACT_TYPE_ID", nullable = false)
    private Long actTypeId;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name="NAME", nullable = false)
    private String name;

    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;


    public ActType() {
    }

    public ActType(Long actTypeId, Timestamp dateCreation,
                   Timestamp dateModified, Long deleted, String description,
                   String name, String userCreation, String userModified) {
        this.actTypeId = actTypeId;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.description = description;
        this.name = name;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    public Long getActTypeId() {
        return actTypeId;
    }

    public void setActTypeId(Long actTypeId) {
        this.actTypeId = actTypeId;
    }

    @JsonIgnore
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

}
