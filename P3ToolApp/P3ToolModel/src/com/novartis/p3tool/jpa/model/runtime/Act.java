package com.novartis.p3tool.jpa.model.runtime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.novartis.p3tool.jpa.model.nestedtypes.Footprint;
import com.novartis.p3tool.jpa.serializer.JsonDateDeserializer;
import com.novartis.p3tool.jpa.serializer.JsonDateSerializer;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;


import javax.xml.bind.annotation.XmlTransient;


@Entity
@NamedQueries({
  @NamedQuery(name = "Act.findAll", query = "select o from Act o")
})
public class Act implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name="P3TOOL_ACTSEQ_GENERATOR", sequenceName="ACT_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="P3TOOL_ACTSEQ_GENERATOR")            
    @Column(name="ACT_ID", nullable = false)
    private Long actId;
    
    @Column(name="ACTOR_FIRST_NAME")
    private String actorFirstName;
    @Column(name="ACTOR_ID", nullable = false)
    private String actorId;
    @Column(name="ACTOR_LAST_NAME")
    private String actorLastName;
    @Column(name="ACT_CREATION_DATE")
    private Timestamp actCreationDate;
    @Column(name="CATEGORY_ID")
    private Long categoryId;
    @Column(name="CATEGORY_NAME")
    private String categoryName;

    @Column(name="FOOTPRINT", length = 4000)
    private String footprint;
    
    
    @Column(name="KEY_FIELD_CATEGORY")
    private String keyFieldCategory;
    @Column(name="KEY_FIELDVALUE_CATEGORY")
    private String keyFieldValueCategory;
    @Column(name="OWNER_FIRST_NAME")
    private String ownerFirstName;
    @Column(name="OWNER_LAST_NAME")
    private String ownerLastName;
    @Column(name="OWNER_USER_ID")
    private String ownerUserId;
    @Column(name="REQUEST_STATUS")
    private String requestStatus;
    @Column(name="REQUEST_TITLE")
    private String requestTitle;
    private String tenant;

    @ManyToOne
    @JoinColumn(name = "ACT_TYPE", insertable=false, updatable=false)
    private ActType actType1;
    
    @Column(name="ACT_TYPE")
    private Long actTypeId;
    
    
    @ManyToOne
    @JoinColumn(name = "REQUEST_ID", insertable=false, updatable=false)
    private Request request;
    
    @Column(name="REQUEST_ID", nullable=false)
    private Long requestId;
    
    
    /* Auditory fields */
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;

    public Act() {
    }

    public Act(Long actId, String actorFirstName, String actorId, String actorLastName, Timestamp actCreationDate,
                Long categoryId, String categoryName, String footprint, String keyFieldCategory, String keyFieldValueCategory,
                String ownerFirstName, String ownerLastName, String ownerUserId, String requestStatus, String requestTitle,
                String tenant, Long actTypeId, Long requestId, Timestamp dateCreation,Timestamp dateModified, Long deleted,
                String userCreation, String userModified) {
        this.actCreationDate = actCreationDate;
        this.actId = actId;
        this.actTypeId = actTypeId;
        this.actorFirstName = actorFirstName;
        this.actorId = actorId;
        this.actorLastName = actorLastName;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.footprint = footprint;
        this.keyFieldCategory = keyFieldCategory;
        this.keyFieldValueCategory = keyFieldValueCategory;
        this.ownerFirstName = ownerFirstName;
        this.ownerLastName = ownerLastName;
        this.ownerUserId = ownerUserId;
        this.requestId = requestId;
        this.requestStatus = requestStatus;
        this.requestTitle = requestTitle;
        this.tenant = tenant;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    public String getActorFirstName() {
        return actorFirstName;
    }

    public void setActorFirstName(String actorFirstName) {
        this.actorFirstName = actorFirstName;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    public String getActorLastName() {
        return actorLastName;
    }

    public void setActorLastName(String actorLastName) {
        this.actorLastName = actorLastName;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getActCreationDate() {
        return actCreationDate;
    }

    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setActCreationDate(Timestamp actCreationDate) {
        this.actCreationDate = actCreationDate;
    }

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }


    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonIgnore
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getKeyFieldCategory() {
        return keyFieldCategory;
    }

    public void setKeyFieldCategory(String keyFieldCategory) {
        this.keyFieldCategory = keyFieldCategory;
    }

    public String getKeyFieldValueCategory() {
        return keyFieldValueCategory;
    }

    public void setKeyFieldValueCategory(String keyFieldValueCategory) {
        this.keyFieldValueCategory = keyFieldValueCategory;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    public String getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(String ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequestTitle() {
        return requestTitle;
    }

    public void setRequestTitle(String requestTitle) {
        this.requestTitle = requestTitle;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }
    
    @JsonIgnore
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }
    
    @JsonIgnore
    @XmlTransient  
    public ActType getActType1() {
        return actType1;
    }

    public void setActType1(ActType actType1) {
        this.actType1 = actType1;
    }

    @JsonIgnore
    @XmlTransient  
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public void setActTypeId(Long actTypeId) {
        this.actTypeId = actTypeId;
    }

    public Long getActTypeId() {
        return actTypeId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setFootprint(Footprint footprint) {
        
        this.footprint = Footprint.serializeFootprint(footprint);
    }

    public Footprint getFootprint() {
        
        return Footprint.deserializeFootprint(this.footprint);
    }
    
}
