package com.novartis.p3tool.jpa.model.runtime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.novartis.p3tool.jpa.serializer.JsonDateDeserializer;
import com.novartis.p3tool.jpa.serializer.JsonDateSerializer;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "Comment.findAll", query = "select o from Comment o")
})
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name="P3TOOL_COMMENTSEQ_GENERATOR", sequenceName="COMMENT_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="P3TOOL_COMMENTSEQ_GENERATOR")
    @Column(name="COMMENT_ID", nullable = false)
    private Long commentId;
    
    
    @Column(name="ADDED_BY")
    private String addedBy;

    @Column(name="COMMENT_TEXT", nullable = false)
    private String commentText;
    @Column(name="DATE_ADDED")
    private Timestamp dateAdded;
    
    
    @ManyToOne
    @JoinColumn(name = "REQUEST_ID", insertable=false, updatable=false)
    private Request request;
    
    @Column(name="REQUEST_ID", nullable=false)
    private Long requestId;
    
    /* Auditory fields */
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;

    public Comment() {
    }

    public Comment(String addedBy, Long commentId, String commentText,
                   Timestamp dateAdded, Timestamp dateCreation,
                   Timestamp dateModified, Long deleted, Request request,
                   String userCreation, String userModified) {
        this.addedBy = addedBy;
        this.commentId = commentId;
        this.commentText = commentText;
        this.dateAdded = dateAdded;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.request = request;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getDateAdded() {
        return dateAdded;
    }

    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    @JsonIgnore
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    @JsonIgnore
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }
    
    @JsonIgnore
    @XmlTransient 
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getRequestId() {
        return requestId;
    }
}
