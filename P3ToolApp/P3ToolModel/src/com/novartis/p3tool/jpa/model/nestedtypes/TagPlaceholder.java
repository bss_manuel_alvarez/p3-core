package com.novartis.p3tool.jpa.model.nestedtypes;

import java.io.Serializable;

import java.util.List;

public class TagPlaceholder implements Serializable {
    private static final long serialVersionUID = 1L;
    
    Long tagId;
    
    String tagName;
    
    String tagType;
    
    String tagDefaultDisplayName;
    
    List<String> tagValues;
        
    
    public TagPlaceholder() {
        super();
    }
    
    public TagPlaceholder(Long tagId, String tagName, String tagType, String tagDefaultDisplayName, List<String> tagValues) {
        super();
        
        this.tagId = tagId;
        this.tagName = tagName;
        this.tagType = tagType;
        this.tagDefaultDisplayName = tagDefaultDisplayName;
        this.tagValues = tagValues;        
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagDefaultDisplayName(String tagDefaultDisplayName) {
        this.tagDefaultDisplayName = tagDefaultDisplayName;
    }

    public String getTagDefaultDisplayName() {
        return tagDefaultDisplayName;
    }

    public void setTagValues(List<String> tagValues) {
        this.tagValues = tagValues;
    }

    public List<String> getTagValues() {
        return tagValues;
    }
}
