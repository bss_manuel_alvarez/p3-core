package com.novartis.p3tool.jpa.model.nestedtypes;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

import java.util.List;

public class ApprovalFlow implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    List<StepPlaceholder> stepList;

    public ApprovalFlow() {
        super();
    }

    public void setStepList(List<StepPlaceholder> stepList) {
        this.stepList = stepList;
    }

    public List<StepPlaceholder> getStepList() {
        return stepList;
    }
    
    public static String serializeApprovalFlow(ApprovalFlow approvalFlow) {
      
        String result = "";

        if (approvalFlow != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                result = mapper.writeValueAsString(approvalFlow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    public static ApprovalFlow deserializeApprovalFlow(String approvalFlowString) {
      
        ApprovalFlow approvalFlow = null;
        
        if (approvalFlowString != null && !approvalFlowString.equals("")) {        
            try {
                ObjectMapper mapper = new ObjectMapper();
                approvalFlow = mapper.readValue(approvalFlowString, ApprovalFlow.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return approvalFlow;
    }
}
