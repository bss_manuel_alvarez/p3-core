package com.novartis.p3tool.jpa.model.nestedtypes;

import java.io.Serializable;

import java.util.List;

public class StepPlaceholder implements Serializable {

    private static final long serialVersionUID = 1L;
    
    String stepType;
    
    Long groupId;
    
    String groupName;
    
    List<UserStep> users;
    
    public StepPlaceholder() {
        super();
    }
    
    public StepPlaceholder(List<UserStep> users, Long groupId, String groupName) {
        super();
        this.users = users;
        this.groupId = groupId;
        this.groupName = groupName;
    }


    public void setStepType(String stepType) {
        this.stepType = stepType;
    }

    public String getStepType() {
        return stepType;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setUsers(List<UserStep> users) {
        this.users = users;
    }

    public List<UserStep> getUsers() {
        return users;
    }
}
