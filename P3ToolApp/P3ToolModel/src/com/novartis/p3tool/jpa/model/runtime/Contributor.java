package com.novartis.p3tool.jpa.model.runtime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.novartis.p3tool.jpa.serializer.JsonDateDeserializer;
import com.novartis.p3tool.jpa.serializer.JsonDateSerializer;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "Contributor.findAll", query = "select o from Contributor o")
})
public class Contributor implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name="P3TOOL_CONTRIBUTORSEQ_GENERATOR", sequenceName="CONTRIBUTOR_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="P3TOOL_CONTRIBUTORSEQ_GENERATOR")
    @Column(name="CONTRIBUTOR_ID", nullable = false)
    private Long contributorId;
    
    
    @Column(name="ADDED_BY_FIRST_NAME")
    private String addedByFirstName;
    @Column(name="ADDED_BY_LAST_NAME")
    private String addedByLastName;
    @Column(name="ADDED_BY_USER_ID", nullable = false)
    private String addedByUserId;   
    @Column(name="DATE_ADDED")
    private Timestamp dateAdded;
    @Column(name="FIRST_NAME")
    private String firstName;
    @Column(name="LAST_NAME")
    private String lastName;
    @Column(name="USER_ID", nullable = false)
    private String userId;

    @ManyToOne
    @JoinColumn(name = "REQUEST_ID", insertable=false, updatable=false)
    private Request request;
    
    @Column(name="REQUEST_ID", nullable=false)
    private Long requestId;
    
    /* Auditory fields */
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;

    public Contributor() {
    }

    public Contributor(String addedByFirstName, String addedByLastName,
                       String addedByUserId, Long contributorId,
                       Timestamp dateAdded, Timestamp dateCreation,
                       Timestamp dateModified, Long deleted, String firstName,
                       String lastName, Request request, String userCreation,
                       String userId, String userModified) {
        this.addedByFirstName = addedByFirstName;
        this.addedByLastName = addedByLastName;
        this.addedByUserId = addedByUserId;
        this.contributorId = contributorId;
        this.dateAdded = dateAdded;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.firstName = firstName;
        this.lastName = lastName;
        this.request = request;
        this.userCreation = userCreation;
        this.userId = userId;
        this.userModified = userModified;
    }

    public String getAddedByFirstName() {
        return addedByFirstName;
    }

    public void setAddedByFirstName(String addedByFirstName) {
        this.addedByFirstName = addedByFirstName;
    }

    public String getAddedByLastName() {
        return addedByLastName;
    }

    public void setAddedByLastName(String addedByLastName) {
        this.addedByLastName = addedByLastName;
    }

    public String getAddedByUserId() {
        return addedByUserId;
    }

    public void setAddedByUserId(String addedByUserId) {
        this.addedByUserId = addedByUserId;
    }

    public Long getContributorId() {
        return contributorId;
    }

    public void setContributorId(Long contributorId) {
        this.contributorId = contributorId;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getDateAdded() {
        return dateAdded;
    }

    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    @JsonIgnore
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }
    
    @JsonIgnore
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonIgnore
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }
    
    @JsonIgnore
    @XmlTransient 
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getRequestId() {
        return requestId;
    }
}
