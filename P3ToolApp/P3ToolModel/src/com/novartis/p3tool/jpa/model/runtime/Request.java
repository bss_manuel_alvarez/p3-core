package com.novartis.p3tool.jpa.model.runtime;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.novartis.p3tool.jpa.model.nestedtypes.ApprovalFlow;
import com.novartis.p3tool.jpa.model.nestedtypes.RequestTags;
import com.novartis.p3tool.jpa.model.nestedtypes.Reviewers;
import com.novartis.p3tool.jpa.serializer.JsonDateDeserializer;
import com.novartis.p3tool.jpa.serializer.JsonDateSerializer;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import javax.persistence.SequenceGenerator;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "Request.findAll", query = "select o from Request o")
})
public class Request implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name="P3TOOL_REQUESTSEQ_GENERATOR", sequenceName="REQUEST_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="P3TOOL_REQUESTSEQ_GENERATOR")
    @Column(name="REQUEST_ID", nullable = false)
    private Long requestId;
    
    @Column(name="APPROVAL_DATE")
    private Timestamp approvalDate;
    @Column(name="APPROVAL_FLOW", length = 4000)
    private String approvalFlow;
    @Column(name="REVIEWERS", length = 4000)
    private String reviewers;    
    @Column(nullable = false)
    private String creator;

    private String description;
    @Column(name="ON_DRAFT")
    private Long onDraft;
    @Column(nullable = false)
    private String requester;
    @Column(name="REQUEST_DATE_CREATION")
    private Timestamp requestDateCreation;
    
    @Column(name="REQUEST_SUBMIT_DATE")
    private Timestamp requestSubmitDate;
    private String status;
    @Column(name="SUBCATEGORY_ID", nullable = false)
    private Long subcategoryId;
    @Column(name="SUBCATEGORY_NAME", nullable = false)
    private String subcategoryName;
    @Column(length = 4000)
    private String tags;
    private String tenant;
    private String title;
    
    
    /* Auditory fields */
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    
    
    /****** Entity Relationship ******/
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "request")
    private List<Comment> commentList;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "request")
    private List<Contributor> contributorList;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "request")
    private List<Act> actList;
    /**********************************/

    public Request() {
    }

    public Request(Timestamp approvalDate, String approvalFlow, String creator,
                   Timestamp dateCreation, Timestamp dateModified,
                   Long deleted, String description, Long onDraft,
                   Timestamp requestDateCreation, Long requestId,
                   Timestamp requestSubmitDate, String requester,
                   String status, Long subcategoryId, String subcategoryName,
                   String tags, String tenant, String title,
                   String userCreation, String userModified) {
        this.approvalDate = approvalDate;
        this.approvalFlow = approvalFlow;
        this.creator = creator;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.description = description;
        this.onDraft = onDraft;
        this.requestDateCreation = requestDateCreation;
        this.requestId = requestId;
        this.requestSubmitDate = requestSubmitDate;
        this.requester = requester;
        this.status = status;
        this.subcategoryId = subcategoryId;
        this.subcategoryName = subcategoryName;
        this.tags = tags;
        this.tenant = tenant;
        this.title = title;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getApprovalDate() {
        return approvalDate;
    }
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setApprovalDate(Timestamp approvalDate) {
        this.approvalDate = approvalDate;
    }

    public ApprovalFlow getApprovalFlow() {
        return ApprovalFlow.deserializeApprovalFlow(this.approvalFlow);
    }

    public void setApprovalFlow(ApprovalFlow approvalFlow) {
        this.approvalFlow = ApprovalFlow.serializeApprovalFlow(approvalFlow);
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @JsonIgnore
    public Timestamp getDateCreation() {
        return dateCreation;
    }
    
    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    public Timestamp getDateModified() {
        return dateModified;
    }
    
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOnDraft() {
        return onDraft;
    }

    public void setOnDraft(Long onDraft) {
        this.onDraft = onDraft;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getRequestDateCreation() {
        return requestDateCreation;
    }
    
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setRequestDateCreation(Timestamp requestDateCreation) {
        this.requestDateCreation = requestDateCreation;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getRequestSubmitDate() {
        return requestSubmitDate;
    }

    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setRequestSubmitDate(Timestamp requestSubmitDate) {
        this.requestSubmitDate = requestSubmitDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Long subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public RequestTags getTags() {
        return RequestTags.deserializeRequestTags(this.tags);
    }

    public void setTags(RequestTags tags) {
        this.tags = RequestTags.serializeRequestTags(tags);
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonIgnore
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    @XmlTransient 
    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public Comment addComment(Comment comment) {
        getCommentList().add(comment);
        comment.setRequest(this);
        return comment;
    }

    public Comment removeComment(Comment comment) {
        getCommentList().remove(comment);
        comment.setRequest(null);
        return comment;
    }
    
    @XmlTransient 
    public List<Contributor> getContributorList() {
        return contributorList;
    }

    public void setContributorList(List<Contributor> contributorList) {
        this.contributorList = contributorList;
    }

    public Contributor addContributor(Contributor contributor) {
        getContributorList().add(contributor);
        contributor.setRequest(this);
        return contributor;
    }

    public Contributor removeContributor(Contributor contributor) {
        getContributorList().remove(contributor);
        contributor.setRequest(null);
        return contributor;
    }
    
    @JsonIgnore
    @XmlTransient 
    public List<Act> getActList() {
        return actList;
    }

    public void setActList(List<Act> actList) {
        this.actList = actList;
    }

    public Act addAct(Act act) {
        getActList().add(act);
        act.setRequest(this);
        return act;
    }

    public Act removeAct(Act act) {
        getActList().remove(act);
        act.setRequest(null);
        return act;
    }

    public void setReviewers(Reviewers reviewers) {
        this.reviewers = Reviewers.serializeReviewers(reviewers);
    }

    public Reviewers getReviewers() {
        return Reviewers.deserializeReviewers(this.reviewers);
    }

    public String getTagsStr() {
        return tags;
    }

    public String getApprovalFlowStr() {
        return approvalFlow;
    }

    public String getReviewersStr() {
        return reviewers;
    }
}
