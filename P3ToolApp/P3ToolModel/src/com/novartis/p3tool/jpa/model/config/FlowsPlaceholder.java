package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.novartis.p3tool.jpa.model.nestedtypes.ApprovalFlow;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "FlowsPlaceholder.findAll", query = "select o from FlowsPlaceholder o")
})
@Table(name = "P3TOOL_FLOWS_PLACEHOLDER")
public class FlowsPlaceholder implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name="FLOW_PLACEHOLDER_DEFINITION", length = 4000)
    private String flowPlaceholderDefinition;
    @Column(name="FLOW_PLACEHOLDER_DESCRIPITON")
    private String flowPlaceholderDescripiton;
    @Id
    @Column(name="FLOW_PLACEHOLDER_ID", nullable = false)
    private Long flowPlaceholderId;
    @Column(name="FLOW_PLACEHOLDER_NAME")
    private String flowPlaceholderName;
    
    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;    
    
    /****** Entity Relationship ******/
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "flowsPlaceholder")
    private List<Category> categoryList;

    public FlowsPlaceholder() {
    }

    public FlowsPlaceholder(Timestamp dateCreation, Timestamp dateModified,
                            Long deleted, String flowPlaceholderDefinition,
                            String flowPlaceholderDescripiton,
                            Long flowPlaceholderId, String flowPlaceholderName,
                            String userCreation, String userModified) {
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.flowPlaceholderDefinition = flowPlaceholderDefinition;
        this.flowPlaceholderDescripiton = flowPlaceholderDescripiton;
        this.flowPlaceholderId = flowPlaceholderId;
        this.flowPlaceholderName = flowPlaceholderName;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public ApprovalFlow getFlowPlaceholderDefinition() {
        return ApprovalFlow.deserializeApprovalFlow(this.flowPlaceholderDefinition);
    }

    public void setFlowPlaceholderDefinition(ApprovalFlow flowPlaceholderDefinition) {
        this.flowPlaceholderDefinition = ApprovalFlow.serializeApprovalFlow(flowPlaceholderDefinition);
    }

    public String getFlowPlaceholderDescripiton() {
        return flowPlaceholderDescripiton;
    }

    public void setFlowPlaceholderDescripiton(String flowPlaceholderDescripiton) {
        this.flowPlaceholderDescripiton = flowPlaceholderDescripiton;
    }

    public Long getFlowPlaceholderId() {
        return flowPlaceholderId;
    }

    public void setFlowPlaceholderId(Long flowPlaceholderId) {
        this.flowPlaceholderId = flowPlaceholderId;
    }

    public String getFlowPlaceholderName() {
        return flowPlaceholderName;
    }

    public void setFlowPlaceholderName(String flowPlaceholderName) {
        this.flowPlaceholderName = flowPlaceholderName;
    }
    
    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }
    
    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public Category addCategory(Category category) {
        getCategoryList().add(category);
        category.setFlowsPlaceholder(this);
        return category;
    }

    public Category removeCategory(Category category) {
        getCategoryList().remove(category);
        category.setFlowsPlaceholder(null);
        return category;
    }

    public String getFlowPlaceholderDefinitionStr() {
        return flowPlaceholderDefinition;
    }
}
