package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "FileTemplates.findAll", query = "select o from FileTemplates o")
})
@Table(name = "P3TOOL_FILE_TEMPLATES")
public class FileTemplates implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="FILE_TEMPLATE_ID", nullable = false)
    private Long fileTemplateId;
    @Column(name="FILE_TEMPLATE_NAME", nullable = false)
    private String fileTemplateName;
    @Column(name="FILE_TEMPLATE_TYPE")
    private String fileTemplateType;
    @Column(name="URL_LINK")
    private String urlLink;
    
    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    
    /****** Entity Relationship ******/
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", insertable=false, updatable=false)
    private Category category;
    
    @Column(name="CATEGORY_ID")
    private Long categoryId;

    public FileTemplates() {
    }

    public FileTemplates(Long categoryId, Timestamp dateCreation,
                         Timestamp dateModified, Long deleted,
                         Long fileTemplateId, String fileTemplateName,
                         String fileTemplateType, String urlLink,
                         String userCreation, String userModified) {
        this.categoryId = categoryId;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.fileTemplateId = fileTemplateId;
        this.fileTemplateName = fileTemplateName;
        this.fileTemplateType = fileTemplateType;
        this.urlLink = urlLink;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getFileTemplateId() {
        return fileTemplateId;
    }

    public void setFileTemplateId(Long fileTemplateId) {
        this.fileTemplateId = fileTemplateId;
    }

    public String getFileTemplateName() {
        return fileTemplateName;
    }

    public void setFileTemplateName(String fileTemplateName) {
        this.fileTemplateName = fileTemplateName;
    }

    public String getFileTemplateType() {
        return fileTemplateType;
    }

    public void setFileTemplateType(String fileTemplateType) {
        this.fileTemplateType = fileTemplateType;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }
}
