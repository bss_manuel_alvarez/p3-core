package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.novartis.p3tool.jpa.model.nestedtypes.RequestTags;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "Category.findAll", query = "select o from Category o")
})
@Table(name = "P3TOOL_CATEGORY")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="CATEGORY_ID", nullable = false)
    private Long categoryId;
    @Column(name="CATEGORY_NAME", nullable = false)
    private String categoryName;
    @Column(name="SHORT_NAME")
    private String shortName;
    
    @Column(name="APPROVERS_AFTER_SUBMIT")
    private Long approversAfterSubmit;
    @Column(name="APPROVERS_AFTER_SUBMIT_BY_APP")
    private Long approversAfterSubmitByApp;
    @Column(name="CHANGES_AFTER_SUBMIT")
    private Long changesAfterSubmit;
    @Column(name="CHANGES_AFTER_SUBMIT_BY_APP")
    private Long changesAfterSubmitByApp;
    @Column(name="ENABLE_ADDITIONAL_APPROVERS")
    private Long enableAdditionalApprovers;
    @Column(name="ENABLE_CREATION_ON_IPHONE")
    private Long enableCreationOnIphone;
    @Column(name="ENABLE_GROUPS")
    private Long enableGroups;
    @Column(name="ENABLE_REVIEWERS")
    private Long enableReviewers;
    @Column(name="FILES_AFTER_SUBMIT")
    private Long filesAfterSubmit;
    @Column(name="FILES_AFTER_SUBMIT_BY_APP")
    private Long filesAfterSubmitByApp;
    @Column(name="REQUESTERS_CREATE_TAGS")
    private Long requestersCreateTags;
    @Column(name="REQUESTERS_EXTEND_LIST")
    private Long requestersExtendList;
    
    @Column(name="FOOTER_INFO")
    private String footerInfo;
    @Column(name="HEADLINE_INFO")
    private String headlineInfo;
    
    @Column(name="TAGS_PLACEHOLDER", length = 4000)
    private String tagsPlaceholder;
    
    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;  
     
    
    /****** Entity Relationship ******/
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "category")
    private List<CategoryType> categoryTypeList;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "category")
    private List<FileTemplates> fileTemplatesList;
    
    @ManyToOne
    @JoinColumn(name = "TENANT_ID", insertable=false, updatable=false)
    private Tenant tenant;
    
    @Column(name="TENANT_ID")
    private Long tenantId;

    @ManyToOne
    @JoinColumn(name = "APPROVAL_FLOW_PLACEHOLDER", insertable=false, updatable=false)
    private FlowsPlaceholder flowsPlaceholder;
    
    @Column(name="APPROVAL_FLOW_PLACEHOLDER")
    private Long flowPlaceholderId;

    public Category() {
    }

    public Category(Long flowPlaceholderId, Long approversAfterSubmit,
                    Long approversAfterSubmitByApp, Long categoryId,
                    String categoryName, Long changesAfterSubmit,
                    Long changesAfterSubmitByApp, Timestamp dateCreation,
                    Timestamp dateModified, Long deleted,
                    Long enableAdditionalApprovers,
                    Long enableCreationOnIphone, Long enableGroups,
                    Long enableReviewers, Long filesAfterSubmit,
                    Long filesAfterSubmitByApp, String footerInfo,
                    String headlineInfo, Long requestersCreateTags,
                    Long requestersExtendList, String shortName,
                    String tagsPlaceholder, Long tenantId, String userCreation,
                    String userModified) {
        this.flowPlaceholderId = flowPlaceholderId;
        this.approversAfterSubmit = approversAfterSubmit;
        this.approversAfterSubmitByApp = approversAfterSubmitByApp;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.changesAfterSubmit = changesAfterSubmit;
        this.changesAfterSubmitByApp = changesAfterSubmitByApp;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.enableAdditionalApprovers = enableAdditionalApprovers;
        this.enableCreationOnIphone = enableCreationOnIphone;
        this.enableGroups = enableGroups;
        this.enableReviewers = enableReviewers;
        this.filesAfterSubmit = filesAfterSubmit;
        this.filesAfterSubmitByApp = filesAfterSubmitByApp;
        this.footerInfo = footerInfo;
        this.headlineInfo = headlineInfo;
        this.requestersCreateTags = requestersCreateTags;
        this.requestersExtendList = requestersExtendList;
        this.shortName = shortName;
        this.tagsPlaceholder = tagsPlaceholder;
        this.tenantId = tenantId;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }


    public Long getApproversAfterSubmit() {
        return approversAfterSubmit;
    }

    public void setApproversAfterSubmit(Long approversAfterSubmit) {
        this.approversAfterSubmit = approversAfterSubmit;
    }

    public Long getApproversAfterSubmitByApp() {
        return approversAfterSubmitByApp;
    }

    public void setApproversAfterSubmitByApp(Long approversAfterSubmitByApp) {
        this.approversAfterSubmitByApp = approversAfterSubmitByApp;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getChangesAfterSubmit() {
        return changesAfterSubmit;
    }

    public void setChangesAfterSubmit(Long changesAfterSubmit) {
        this.changesAfterSubmit = changesAfterSubmit;
    }

    public Long getChangesAfterSubmitByApp() {
        return changesAfterSubmitByApp;
    }

    public void setChangesAfterSubmitByApp(Long changesAfterSubmitByApp) {
        this.changesAfterSubmitByApp = changesAfterSubmitByApp;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getEnableAdditionalApprovers() {
        return enableAdditionalApprovers;
    }

    public void setEnableAdditionalApprovers(Long enableAdditionalApprovers) {
        this.enableAdditionalApprovers = enableAdditionalApprovers;
    }

    public Long getEnableCreationOnIphone() {
        return enableCreationOnIphone;
    }

    public void setEnableCreationOnIphone(Long enableCreationOnIphone) {
        this.enableCreationOnIphone = enableCreationOnIphone;
    }

    public Long getEnableGroups() {
        return enableGroups;
    }

    public void setEnableGroups(Long enableGroups) {
        this.enableGroups = enableGroups;
    }

    public Long getEnableReviewers() {
        return enableReviewers;
    }

    public void setEnableReviewers(Long enableReviewers) {
        this.enableReviewers = enableReviewers;
    }

    public Long getFilesAfterSubmit() {
        return filesAfterSubmit;
    }

    public void setFilesAfterSubmit(Long filesAfterSubmit) {
        this.filesAfterSubmit = filesAfterSubmit;
    }

    public Long getFilesAfterSubmitByApp() {
        return filesAfterSubmitByApp;
    }

    public void setFilesAfterSubmitByApp(Long filesAfterSubmitByApp) {
        this.filesAfterSubmitByApp = filesAfterSubmitByApp;
    }

    public String getFooterInfo() {
        return footerInfo;
    }

    public void setFooterInfo(String footerInfo) {
        this.footerInfo = footerInfo;
    }

    public String getHeadlineInfo() {
        return headlineInfo;
    }

    public void setHeadlineInfo(String headlineInfo) {
        this.headlineInfo = headlineInfo;
    }

    public Long getRequestersCreateTags() {
        return requestersCreateTags;
    }

    public void setRequestersCreateTags(Long requestersCreateTags) {
        this.requestersCreateTags = requestersCreateTags;
    }

    public Long getRequestersExtendList() {
        return requestersExtendList;
    }

    public void setRequestersExtendList(Long requestersExtendList) {
        this.requestersExtendList = requestersExtendList;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public RequestTags getTagsPlaceholder() {
        return RequestTags.deserializeRequestTags(this.tagsPlaceholder);
    }

    public void setTagsPlaceholder(RequestTags tagsPlaceholder) {
        this.tagsPlaceholder = RequestTags.serializeRequestTags(tagsPlaceholder);
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public List<CategoryType> getCategoryTypeList() {
        return categoryTypeList;
    }

    public void setCategoryTypeList(List<CategoryType> categoryTypeList) {
        this.categoryTypeList = categoryTypeList;
    }

    public CategoryType addCategoryType(CategoryType categoryType) {
        getCategoryTypeList().add(categoryType);
        categoryType.setCategory(this);
        return categoryType;
    }

    public CategoryType removeCategoryType(CategoryType categoryType) {
        getCategoryTypeList().remove(categoryType);
        categoryType.setCategory(null);
        return categoryType;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @JsonIgnore
    @XmlTransient 
    public List<FileTemplates> getFileTemplatesList() {
        return fileTemplatesList;
    }

    public void setFileTemplatesList(List<FileTemplates> fileTemplatesList) {
        this.fileTemplatesList = fileTemplatesList;
    }

    public FileTemplates addFileTemplates(FileTemplates fileTemplates) {
        getFileTemplatesList().add(fileTemplates);
        fileTemplates.setCategory(this);
        return fileTemplates;
    }

    public FileTemplates removeFileTemplates(FileTemplates fileTemplates) {
        getFileTemplatesList().remove(fileTemplates);
        fileTemplates.setCategory(null);
        return fileTemplates;
    }

    public FlowsPlaceholder getFlowsPlaceholder() {
        return flowsPlaceholder;
    }

    public void setFlowsPlaceholder(FlowsPlaceholder flowsPlaceholder) {
        this.flowsPlaceholder = flowsPlaceholder;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setFlowPlaceholderId(Long flowPlaceholderId) {
        this.flowPlaceholderId = flowPlaceholderId;
    }

    public Long getFlowPlaceholderId() {
        return flowPlaceholderId;
    }

    public String getTagsPlaceholderStr() {
        return tagsPlaceholder;
    }
}
