package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "CategoryType.findAll", query = "select o from CategoryType o")
})
@Table(name = "P3TOOL_CATEGORY_TYPE")
public class CategoryType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name="CATEGORY_TYPE_DESCRIPTION", nullable = false)
    private String categoryTypeDescription;
    @Id
    @Column(name="CATEGORY_TYPE_ID", nullable = false)
    private Long categoryTypeId;
    @Column(name="CATEGORY_TYPE_NAME")
    private String categoryTypeName;
    
    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;  
    
    
    /****** Entity Relationship ******/
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", insertable=false, updatable=false)
    private Category category;
    
    @Column(name="CATEGORY_ID")
    private Long categoryId;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "categoryType")
    private List<Disclaimer> disclaimerList;

    public CategoryType() {
    }

    public CategoryType(Long categoryId, String categoryTypeDescription,
                        Long categoryTypeId, String categoryTypeName,
                        Timestamp dateCreation, Timestamp dateModified,
                        Long deleted, String userCreation,
                        String userModified) {
        this.categoryId = categoryId;
        this.categoryTypeDescription = categoryTypeDescription;
        this.categoryTypeId = categoryTypeId;
        this.categoryTypeName = categoryTypeName;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }


    public String getCategoryTypeDescription() {
        return categoryTypeDescription;
    }

    public void setCategoryTypeDescription(String categoryTypeDescription) {
        this.categoryTypeDescription = categoryTypeDescription;
    }

    public Long getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(Long categoryTypeId) {
        this.categoryTypeId = categoryTypeId;
    }

    public String getCategoryTypeName() {
        return categoryTypeName;
    }

    public void setCategoryTypeName(String categoryTypeName) {
        this.categoryTypeName = categoryTypeName;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @JsonIgnore
    @XmlTransient 
    public List<Disclaimer> getDisclaimerList() {
        return disclaimerList;
    }

    public void setDisclaimerList(List<Disclaimer> disclaimerList) {
        this.disclaimerList = disclaimerList;
    }

    public Disclaimer addDisclaimer(Disclaimer disclaimer) {
        getDisclaimerList().add(disclaimer);
        disclaimer.setCategoryType(this);
        return disclaimer;
    }

    public Disclaimer removeDisclaimer(Disclaimer disclaimer) {
        getDisclaimerList().remove(disclaimer);
        disclaimer.setCategoryType(null);
        return disclaimer;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }
}
