package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "Tag.findAll", query = "select o from Tag o")
})
@Table(name = "P3TOOL_TAG")
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="TAG_ID", nullable = false)
    private Long tagId;

    @Column(name="TAG_DEFAULT_DISPLAY_NAME", nullable = false)
    private String tagDefaultDisplayName;
    @Column(name="TAG_DEFAULT_VALUE")
    private String tagDefaultValue;

    @Column(name="TAG_NAME", nullable = false)
    private String tagName;
    @Column(name="TAG_TOOLTIP")
    private String tagTooltip;
    @Column(name="TAG_TYPE", nullable = false)
    private String tagType;
    
    @Column(name="COMMON_TAG")
    private Long commonTag;
    @Column(name="USER_OWNER")
    private String userOwner;
    
    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    
    /****** Entity Relationship ******/
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "tag")
    private List<TagValues> tagValuesList;

    public Tag() {
    }

    public Tag(Long commonTag, Timestamp dateCreation, Timestamp dateModified,
               Long deleted, String tagDefaultDisplayName,
               String tagDefaultValue, Long tagId, String tagName,
               String tagTooltip, String tagType, String userCreation,
               String userModified, String userOwner) {
        this.commonTag = commonTag;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.tagDefaultDisplayName = tagDefaultDisplayName;
        this.tagDefaultValue = tagDefaultValue;
        this.tagId = tagId;
        this.tagName = tagName;
        this.tagTooltip = tagTooltip;
        this.tagType = tagType;
        this.userCreation = userCreation;
        this.userModified = userModified;
        this.userOwner = userOwner;
    }

    public Long getCommonTag() {
        return commonTag;
    }

    public void setCommonTag(Long commonTag) {
        this.commonTag = commonTag;
    }

    @JsonIgnore
    @XmlTransient  
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient  
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient  
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getTagDefaultDisplayName() {
        return tagDefaultDisplayName;
    }

    public void setTagDefaultDisplayName(String tagDefaultDisplayName) {
        this.tagDefaultDisplayName = tagDefaultDisplayName;
    }

    public String getTagDefaultValue() {
        return tagDefaultValue;
    }

    public void setTagDefaultValue(String tagDefaultValue) {
        this.tagDefaultValue = tagDefaultValue;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagTooltip() {
        return tagTooltip;
    }

    public void setTagTooltip(String tagTooltip) {
        this.tagTooltip = tagTooltip;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient  
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public String getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(String userOwner) {
        this.userOwner = userOwner;
    }

    @JsonIgnore
    @XmlTransient  
    public List<TagValues> getTagValuesList() {
        return tagValuesList;
    }

    public void setTagValuesList(List<TagValues> tagValuesList) {
        this.tagValuesList = tagValuesList;
    }

    public TagValues addTagValues(TagValues tagValues) {
        getTagValuesList().add(tagValues);
        tagValues.setTag(this);
        return tagValues;
    }

    public TagValues removeTagValues(TagValues tagValues) {
        getTagValuesList().remove(tagValues);
        tagValues.setTag(null);
        return tagValues;
    }
}
