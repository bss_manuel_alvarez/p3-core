package com.novartis.p3tool.jpa.model.nestedtypes;

public class UserStep extends UserData  {
    private static final long serialVersionUID = 1L;
    
    String status;
    
    String businessRolePlaceholder;
    
    String userRole;
    
    String location;
    

    public UserStep() {
        super();
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setBusinessRolePlaceholder(String businessRolePlaceholder) {
        this.businessRolePlaceholder = businessRolePlaceholder;
    }

    public String getBusinessRolePlaceholder() {
        return businessRolePlaceholder;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
