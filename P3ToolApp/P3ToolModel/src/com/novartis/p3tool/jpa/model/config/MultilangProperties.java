package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.novartis.p3tool.jpa.serializer.JsonDateDeserializer;
import com.novartis.p3tool.jpa.serializer.JsonDateSerializer;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "MultilangProperties.findAll", query = "select o from MultilangProperties o")
})
@Table(name = "P3TOOL_MULTILANG_PROPERTIES")
public class MultilangProperties implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(nullable = false)
    private Long id;
    @Column(name="LANGUAGE_LOC")
    private String languageLoc;
    @Column(name="MODULE_NAME", nullable = false)
    private String moduleName;
    @Column(name="MULTILANGUAGE_BUNDLE")
    private String multilanguageBundle;
    
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;

    public MultilangProperties() {
    }

    public MultilangProperties(Timestamp dateCreation, Timestamp dateModified,
                               Long deleted, Long id, String languageLoc,
                               String moduleName, String userCreation,
                               String userModified) {
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.id = id;
        this.languageLoc = languageLoc;
        this.moduleName = moduleName;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getDateCreation() {
        return dateCreation;
    }
    
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Timestamp getDateModified() {
        return dateModified;
    }
    
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLanguageLoc() {
        return languageLoc;
    }

    public void setLanguageLoc(String languageLoc) {
        this.languageLoc = languageLoc;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getMultilanguageBundle() {
        return multilanguageBundle;
    }

    public void setMultilanguageBundle(String multilanguageBundle) {
        this.multilanguageBundle = multilanguageBundle;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }
}
