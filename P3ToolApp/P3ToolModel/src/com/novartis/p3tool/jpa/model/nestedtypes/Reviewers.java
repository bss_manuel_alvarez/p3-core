package com.novartis.p3tool.jpa.model.nestedtypes;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

import java.util.List;

public class Reviewers implements Serializable {
    private static final long serialVersionUID = 1L;
    
    List<UserStep> users;

    public Reviewers() {
        super();
    }

    public void setUsers(List<UserStep> users) {
        this.users = users;
    }

    public List<UserStep> getUsers() {
        return users;
    }
    
    public static String serializeReviewers(Reviewers reviewers) {
      
        String result = "";

        if (reviewers != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                result = mapper.writeValueAsString(reviewers);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }
    
    public static Reviewers deserializeReviewers(String reviewersString) {
      
        Reviewers reviewers = null;
        
        if (reviewersString != null && !reviewersString.equals("")) {        
            try {
                ObjectMapper mapper = new ObjectMapper();
                reviewers = mapper.readValue(reviewersString, Reviewers.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return reviewers;
    }
}
