package com.novartis.p3tool.jpa.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
  @NamedQuery(name = "Disclaimer.findAll", query = "select o from Disclaimer o")
})
@Table(name = "P3TOOL_DISCLAIMER")
public class Disclaimer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="DISCLAIMER_ID", nullable = false)
    private Long disclaimerId;
    @Column(name="DISCLAIMER_TEXT", length = 4000)
    private String disclaimerText;
    @Column(name="LANGUAGE_LOC")
    private String languageLoc;
    
    /* Auditory fields */
    @Column(name="USER_CREATION")
    private String userCreation;
    @Column(name="USER_MODIFIED")
    private String userModified;
    @Column(name="DATE_CREATION")
    private Timestamp dateCreation;
    @Column(name="DATE_MODIFIED")
    private Timestamp dateModified;
    private Long deleted;
    
    /****** Entity Relationship ******/
    @ManyToOne
    @JoinColumn(name = "CATEGORY_TYPE_ID", insertable=false, updatable=false)
    private CategoryType categoryType;
    
    @Column(name="CATEGORY_TYPE_ID")
    private Long categoryTypeyId;

    public Disclaimer() {
    }

    public Disclaimer(Long categoryTypeyId, Timestamp dateCreation,
                      Timestamp dateModified, Long deleted, Long disclaimerId,
                      String disclaimerText, String languageLoc,
                      String userCreation, String userModified) {
        this.categoryTypeyId = categoryTypeyId;
        this.dateCreation = dateCreation;
        this.dateModified = dateModified;
        this.deleted = deleted;
        this.disclaimerId = disclaimerId;
        this.disclaimerText = disclaimerText;
        this.languageLoc = languageLoc;
        this.userCreation = userCreation;
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @JsonIgnore
    @XmlTransient 
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getDisclaimerId() {
        return disclaimerId;
    }

    public void setDisclaimerId(Long disclaimerId) {
        this.disclaimerId = disclaimerId;
    }

    public String getDisclaimerText() {
        return disclaimerText;
    }

    public void setDisclaimerText(String disclaimerText) {
        this.disclaimerText = disclaimerText;
    }

    public String getLanguageLoc() {
        return languageLoc;
    }

    public void setLanguageLoc(String languageLoc) {
        this.languageLoc = languageLoc;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    @JsonIgnore
    @XmlTransient 
    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    @JsonIgnore
    @XmlTransient 
    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public void setCategoryTypeyId(Long categoryTypeyId) {
        this.categoryTypeyId = categoryTypeyId;
    }

    public Long getCategoryTypeyId() {
        return categoryTypeyId;
    }
}
