package com.novartis.p3tool.jpa.model.nestedtypes;

import java.io.Serializable;

public class UserData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    String userId;
    
    String firstName;
    
    String lastName;
    
    public UserData() {
        super();
    }
    
    public UserData(String userId, String firstName, String lastName) {        
        super();
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
}
