package com.novartis.p3tool.jpa.test;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.novartis.p3tool.jpa.facade.runtime.ActSession;

import com.novartis.p3tool.jpa.model.runtime.Act;

import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ActSessionClient {
    public static void main(String [] args) {
        try {
            final Context context = getInitialContext();
            ActSession actSession = (ActSession)context.lookup("P3ToolApp-P3ToolModel-ActSession#com.novartis.p3tool.jpa.facade.runtime.ActSession");
           
            String actString = "{\n" + 
            "	\"footprint\": {\n" + 
            "		\"userList\": [{\n" + 
            "			\"userId\": \"test1\",\n" + 
            "			\"firstName\": \"test2\",\n" + 
            "			\"lastName\": \"test3\"\n" + 
            "		},\n" + 
            "		{\n" + 
            "			\"userId\": \"test1\",\n" + 
            "			\"firstName\": \"test2\",\n" + 
            "			\"lastName\": \"test3\"\n" + 
            "		}]\n" + 
            "	}\n" + 
            "}";
                
            ObjectMapper om= new ObjectMapper();
            Act act1 = om.readValue(actString, Act.class);
            System.out.println();
    /*
            
            String foot = "{\n" + 
            "	\"userList\": [{\n" + 
            "		\"userId\": \"test1\",\n" + 
            "		\"firstName\": \"test2\",\n" + 
            "		\"lastName\": \"test3\"\n" + 
            "	},\n" + 
            "	{\n" + 
            "		\"userId\": \"test1\",\n" + 
            "		\"firstName\": \"test2\",\n" + 
            "		\"lastName\": \"test3\"\n" + 
            "	}]\n" + 
            "}";
            
            Footprint footprint = new Footprint();
            
            
            footprint = FootprintSerializer.deserializeFootprint(foot);
            
            Act act = new Act(null, 2L, null, 1L, 1L, "ActorName", "actorid", "actorLastname", 3L, "categoryName", null, null, null,
                    footprint, "test", "test", "owner", "ownerlatname", "ownerId", "status",
                              "title", "tenant", "userCreation", "userModified");
            
            
            
            actSession.persistAct(act);

*/
          
            
            
            
            for (Act act : (List<Act>)actSession.getActFindAll()) {
                printAct(act);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void printAct(Act act) {
        System.out.println( "actorFirstName = " + act.getActorFirstName() );
        System.out.println( "actorId = " + act.getActorId() );
        System.out.println( "actorLastName = " + act.getActorLastName() );
        System.out.println( "actCreationDate = " + act.getActCreationDate() );
        System.out.println( "actId = " + act.getActId() );
        System.out.println( "categoryId = " + act.getCategoryId() );
        System.out.println( "categoryName = " + act.getCategoryName() );
        System.out.println( "dateCreation = " + act.getDateCreation() );
        System.out.println( "dateModified = " + act.getDateModified() );
        System.out.println( "deleted = " + act.getDeleted() );
        System.out.println( "keyFieldCategory = " + act.getKeyFieldCategory() );
        System.out.println( "keyFieldValueCategory = " + act.getKeyFieldValueCategory() );
        System.out.println( "ownerFirstName = " + act.getOwnerFirstName() );
        System.out.println( "ownerLastName = " + act.getOwnerLastName() );
        System.out.println( "ownerUserId = " + act.getOwnerUserId() );
        System.out.println( "requestStatus = " + act.getRequestStatus() );
        System.out.println( "requestTitle = " + act.getRequestTitle() );
        System.out.println( "tenant = " + act.getTenant() );
        System.out.println( "userCreation = " + act.getUserCreation() );
        System.out.println( "userModified = " + act.getUserModified() );
        System.out.println( "actType1 = " + act.getActType1() );
        System.out.println( "footprint userId= " + act.getFootprint());
        System.out.println( "footprint userId= " + act.getFootprint().getUserList().get(0).getUserId() );
        //System.out.println( "actTypeId = " + act.getActTypeId() );
        System.out.println( "requestId = " + act.getRequestId() );
    }


    private static Context getInitialContext() throws NamingException {
        Hashtable env = new Hashtable();
        // WebLogic Server 10.x connection details
        env.put( Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory" );
        env.put(Context.PROVIDER_URL, "t3://soa-server:7003");
        return new InitialContext( env );
    }
}
