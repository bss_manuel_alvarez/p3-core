package com.novartis.p3tool.jpa.test;

import com.novartis.p3tool.jpa.facade.runtime.RequestSession;

import com.novartis.p3tool.jpa.model.runtime.Request;

import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class RequestSessionClient {
    public static void main(String [] args) {
        try {
            final Context context = getInitialContext();
            RequestSession requestSession = (RequestSession)context.lookup("P3ToolApp-P3ToolModel-RequestSession#com.novartis.p3tool.jpa.facade.runtime.RequestSession");
            for (Request request : (List<Request>)requestSession.getRequestFindAll()) {
                printRequest(request);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void printRequest(Request request) {
        System.out.println( "approvalDate = " + request.getApprovalDate() );
        System.out.println( "approvalFlow Step Type 0 = " + request.getApprovalFlow().getStepList().get(0).getUsers().get(0).getUserId());
        
        
        System.out.println( "creator = " + request.getCreator() );
        System.out.println( "dateCreation = " + request.getDateCreation() );
        System.out.println( "dateModified = " + request.getDateModified() );
        System.out.println( "deleted = " + request.getDeleted() );
        System.out.println( "description = " + request.getDescription() );
        System.out.println( "onDraft = " + request.getOnDraft() );
        System.out.println( "requester = " + request.getRequester() );
        System.out.println( "requestDateCreation = " + request.getRequestDateCreation() );
        System.out.println( "requestId = " + request.getRequestId() );
        System.out.println( "requestSubmitDate = " + request.getRequestSubmitDate() );
        System.out.println( "status = " + request.getStatus() );
        System.out.println( "subcategoryId = " + request.getSubcategoryId() );
        System.out.println( "subcategoryName = " + request.getSubcategoryName() );
        System.out.println( "tags = " + request.getTags().toString() );
        System.out.println( "tenant = " + request.getTenant() );
        System.out.println( "title = " + request.getTitle() );
        System.out.println( "userCreation = " + request.getUserCreation() );
        System.out.println( "userModified = " + request.getUserModified() );
        //System.out.println( "attachmentList = " + request.getAttachmentList() );
        //System.out.println( "commentList = " + request.getCommentList() );
        //System.out.println( "contributorList = " + request.getContributorList() );
        //System.out.println( "actList = " + request.getActList() );
    }

    private static Context getInitialContext() throws NamingException {
        Hashtable env = new Hashtable();
        // WebLogic Server 10.x connection details
        env.put( Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory" );
        env.put(Context.PROVIDER_URL, "t3://soa-server:7003");
        return new InitialContext( env );
    }
}
